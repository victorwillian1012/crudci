function getData(id, produto, preco, descricao){

    var x = document.getElementsByClassName("invisivel");
    var i;
    for (i = 0; i < x.length; i++) {
        x[i].style.display = 'inline-block';
    }

    document.getElementById('buttonCancel').style.display = 'inline-block';

    document.getElementById('formInicial').action = "produtos/atualizar/<?= $produto['id']); ?>";
    document.getElementById('buttonSubmit').innerHTML = "Salvar Alterações";

    document.getElementById('txtId').value = id;
    document.getElementById('txtNome').value = produto;
    document.getElementById('txtPreco').value = preco;
    document.getElementById('txtDescricao').value = descricao;
}

function cancelUpdate(){
    var x = document.getElementsByClassName("invisivel");
    var i;
    for (i = 0; i < x.length; i++) {
        x[i].style.display = 'none';
    }

    document.getElementById('buttonCancel').style.display = 'none';

    document.getElementById('formInicial').action = "http://localhost/crudci/Produtos/cadastrar";
    document.getElementById('buttonSubmit').innerHTML = "Cadastrar";

    document.getElementById('txtId').value = "";
    document.getElementById('txtNome').value = "";
    document.getElementById('txtPreco').value = "";
    document.getElementById('txtDescricao').value = "";
}