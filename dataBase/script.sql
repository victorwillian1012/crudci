create database if not exists bdprodutos
default charset utf8
default collate utf8_general_ci;

use bdprodutos;

create table if not exists tblprodutos(
	id int not null auto_increment primary key,
	nome varchar(70) not null,
	preco float not null,
	descricao varchar(100) not null
)default charset utf8;

select * from tblprodutos;