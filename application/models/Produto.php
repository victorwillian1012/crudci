<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produto extends CI_Model {

	//CONSTRUTOR 
	public function __construct(){
		parent::__construct();
	}

	//FUNÇÃO PARA INSERIR OS PRODUTOS
  	public function insert($data){

		//INSERIR NA TABELA PRODUTOS TODOS OS DADOS DO ARRAY $DATA
		$this->db->insert('tblprodutos',$data);
	}
	
	//FUNÇÃO PARA LISTAR TODOS OS PRODUTOS
	public function select(){

		//PEGAR DA TABELA PRODUTOS TODOS OS PRODUTOS E ARMAZENAR EM UM ARRAY, RETORNAR ESSE ARRAY
		return $this->db->get("tblprodutos")->result_array();
	}

	//FUNÇÃO PARA DELETAR PRODUTO	
	public function delete($id){
		$this->db->where('id', $id);
		$this->db->delete('tblprodutos');
		return true;
	}

	//FUNÇÃO PARA ATUALIZAR TODOS OS PRODUTOS
	public function update($id , $datas){
	

		$this->db->where('id',$id);
		return $this->db->update('tblprodutos',$datas);
	}

}