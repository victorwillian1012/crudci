<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		//$this->load->helper('url');

		//$this->load->helper(array('url','form'));
	}

	public function index(){
		//CARREGANDO A MODEL
		$this->load->model('Produto');

		//LISTANDO TODOS OS PRODUTOS
		$lista = $this->Produto->select();

		//PARA PASSAR A LISTA DOS PRODUTOS
		$dados = array('produtos' => $lista);

		//CARREGANDO A NOSSA VIEW INICIAL PASSANDO OS DADOS
		$this->load->view('home',$dados);

	}

	public function cadastrar(){

		//VALIDANDO OS CAMPOS
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nome','nome','required');
		$this->form_validation->set_rules('preco','preco','required');
		$this->form_validation->set_rules('descricao','descricao','required');

		//ARMAZENANDO A VALIDAÇÃO DO CAMPO
		$sucesso = $this->form_validation->run();

		//SE OS CAMPOS ESTIVEREM PREENCHIDOS CORRETAMENTE EU CHAMO O METODO INSERT DA MODEL
		if($sucesso){

		//PEGANDO TODOS OS DADOS DIGITADOS PELO USUARIO
		$nome = $this->input->post('nome');
		$preco = $this->input->post('preco');
		$descricao = $this->input->post('descricao');

		//CARREGANDO A MODEL RESPONSAVEL POR CADASTRAR
		$this->load->model('Produto');

		//ARMAZENADO TODOS ESSES DADOS DENTRO DE UM ARRAY
		$data  =  array ( 
			'nome'=>$nome , 
			'preco'=>$preco , 
			'descricao'=>$descricao 
		);
	
		//CHAMANDO O METODO INSERT DA MODEL PASSANDO OS DADOS QUE SERAO INSERIDOS
		$this->Produto->insert($data);
	
		//REDIRENCIONANDO O NOSSO USUARIO DE VOLTA A PAGINA INICIAL
		redirect(base_url());
		}
		else{

			redirect(base_url());
		}
		
	}

	public function deletar($id){
		//CHAMAR A MODEL
		$this->load->model('Produto');

		$this->Produto->delete($id);

		redirect(base_url());
	}

	public function atualizar(){
		//VALIDANDO OS CAMPOS
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nome','nome','required');
		$this->form_validation->set_rules('preco','preco','required');
		$this->form_validation->set_rules('descricao','descricao','required');

		//ARMAZENANDO A VALIDAÇÃO DO CAMPO
		$sucesso = $this->form_validation->run();

		//SE OS CAMPOS ESTIVEREM PREENCHIDOS CORRETAMENTE EU CHAMO O METODO INSERT DA MODEL
		if($sucesso){

		//CARREGANDO A MODEL RESPONSAVEL POR CADASTRAR
		$this->load->model('Produto');
	
		//PEGANDO O ID DO PRODUTO A SER ATUALIZADO
		$id = $this->input->post('id');

		//PEGANDO TODOS OS DADOS QUE SEÃO ATUALIZADO
		$datas = array(
			'nome' => $this->input->post('nome'),
			'preco' => $this->input->post('preco'),
			'descricao' => $this->input->post('descricao')
		);

		//CHAMANDO O METODO INSERT DA MODEL PASSANDO OS DADOS QUE SERAO INSERIDOS
		$this->Produto->update($id, $datas);
	
		//REDIRENCIONANDO O NOSSO USUARIO DE VOLTA A PAGINA INICIAL
		redirect(base_url());
		}
		else{

			redirect(base_url());
		}
		
	}

}
