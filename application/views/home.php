<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
?><!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css');?>">
	  <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <title>Produtos</title>
  </head>
  <body>
    <main>
      <div class="container">
        <header class="jumbotron">
          <h1>Cadastrar Produtos</h1>
        </header> <!-- jumbotron -->

          <form id="formInicial" action="<?php echo base_url('Produtos/cadastrar'); ?>" method="post">
          <div class="form-group">
            <label class="control-label input-label invisivel" >ID :</label>
            <input id="txtId" type="text" name="id" class="filders invisivel" readonly>
            <label class="control-label input-label" >Produto :</label>
            <input id="txtNome" type="text" name="nome" class="filders" required>
            <label class="control-label input-label">Preço :</label>
            <input id="txtPreco" type = "number" step=".01" name="preco" class="filders" required>
            <label class="control-label input-label">Descrição :</label>
            <input id="txtDescricao" type="text" name="descricao" class="filders" required>
          </div><!-- form-group -->
          <button id="buttonSubmit" type="submit" value = "Cadastrar" class="btn btn-dark">Cadastrar</button>
        </form>
        <button id="buttonCancel" class="btn btn-danger" onclick="cancelUpdate()">Cancelar</button>
        <div class = "listaProdutos"> 
          <h2 class="produtos"> Todos os Produtos</h2>

          <!-- TABELA -->
          <table class = "table table-striped"> 
            <!-- CABEÇALHO DA TABELA -->
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Produtos</td>
                <th scope="col">Preço</td>
                <th scope="col">Descrição</td>
              </tr>
            </thead>
            <!-- CORPO DA TABELA -->
            <tbody>
              <?php foreach ($produtos as $produto) :  ?>
              <tr>
                <td id="tblID"><?= $produto['id'] ?></td>
                <td id="tblProduto"><?= $produto['nome']?></td>
                <td id="tblPreco"><?= reais($produto['preco']) ?></td>
                <td id="tblDescricao"><?= $produto['descricao'] ?></td>

                <td><button class="btn btn-primary" onclick="getData(<?= $produto['id'] ?>,'<?= $produto['nome']?>', <?= $produto['preco'] ?>,'<?= $produto['descricao']?>')">Atualizar</button></td>
                <td><a href="<?= base_url('produtos/deletar/' . $produto['id'])?>" class="btn btn-danger">
                  <span><i class="fas fa-trash-alt"></i></span>  
                </a></td>
              </tr>
            <?php endforeach ?>
            </tbody>
          </table><!-- table -->
        </div><!-- listaProdutos -->
        
      </div> <!-- container -->
    </main>

    <!-- Optional JavaScript -->
  
	<script src="<?php echo base_url('assets/js/script.js');?>"></script>
  </body>
</html>